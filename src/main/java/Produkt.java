public class Produkt implements Comparable<Produkt> {
    String shopURL, productName;
    double price;

    public Produkt(String shopURL, String productName, double price) {
        this.shopURL = shopURL;
        this.productName = productName;
        this.price = price;
    }

    public String getShopURL() {
        return shopURL;
    }

    public void setShopURL(String shopURL) {
        this.shopURL = shopURL;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "shopURL='" + shopURL + '\'' +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public int compareTo(Produkt o) {
        if (price < o.getPrice()) return -1;
        else if (price > o.getPrice()) return 1;
        else return 0;
    }
}
