import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Ceneo {

    public static void main(String[] args) throws IOException {

        List<Produkt> lista = new ArrayList<>();

        Document doc = Jsoup.connect("https://www.ceneo.pl/39052947").timeout(10000).userAgent("Mozilla").get();
        Elements productTables = doc.select("table.product-offers.js_product-offers");
        StringBuilder sb = new StringBuilder();
        Element priceElement;
        String shopURL, gaProductName, value, penny;
        Double price;
        for (Element table : productTables) {
            Elements rows = table.select("tr.product-offer.js_product-offer");
            for (Element row : rows) {
                shopURL = row.attr("data-shopurl");
                gaProductName = row.attr("data-gaproductname");
                priceElement = row.select("td.cell-price > a > span.price-format > span.price").first();
                value = priceElement.select("span.value").first().text();
                penny = priceElement.select("span.penny").first().text();
                price = Double.valueOf(value.concat(penny.replace(",", ".")));
                sb.append(shopURL).append(",").append(gaProductName.trim()).append(",").append(price).append("\n");
                lista.add(new Produkt(shopURL,gaProductName,price));
            }
        }

//        System.out.println(sb);
        Collections.sort(lista);
        for (Produkt p : lista){
            System.out.println(p);
        }
    }

}
